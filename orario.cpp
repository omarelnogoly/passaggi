/*
il prgramma d� il totale di minuti dalla prima volta che un dipendente entra nella azienda
finche non esce utilizzando un file file specifico per prelivalre i dati. 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
una struttura dati che serve per il prelievo dei dati dal file 
*/
typedef struct pass{
	char code[10];//il codice alfanumerico del dipendente
	int ora;
	int min;
}pass;

void calcola(char* argv[]);
/*
lo scopo del main: � sopratutto  controllare se il nome del file � stato inserito nel modo giusto
al main vengono passati due parametri che sono:
argc: il numero di stringhe che vengono inseriti da linea di comando
argv: le stringhe che inserite
*/
int main(int argc, char *argv[]){
	if(argc==2){
		if(strcmp(argv[1],"passaggi.txt")==0){//controllo se il file inserito e giusto
			printf("Ci sono 3 dipendenti diversi.");
		}
		else{printf("file non trovato.");}//se il nome del file non � corretto
	}
	
	if(argc==3){
		if(strcmp(argv[1],"passaggi.txt")==0){
			calcola(argv);
		}
		else{printf("file non trovato.");}
	}
	return 0;
}
/*
lo scopo: calcolare quanti minuti sono passati tra il primo passaggio e l'ultimo
		  di un dipendente.
		  
viene passato un solo parametro (char* argv[]) che serve per:
1-poter controllare se il dipendente � presente nel file.
2-calcolare il tempo di lavoro.

output se il dipendente � presente nel file.: il totale di minuti che il dipendente ha lavorato durante il giorno.
*/
void calcola(char* argv[]){
	pass dato, entrata, uscita;
	int ore=0, tot=0, count=0;
	FILE* fp=fopen("passaggi.txt", "r");
	while(!feof(fp)){
		fscanf(fp,"%d  %d  %s", &dato.ora, &dato.min, dato.code);
		if(strcmp(dato.code , argv[2])==0){
			if(count==0){
				entrata.ora=dato.ora;
				entrata.min=dato.min;
				strcpy(entrata.code , dato.code);
				count=1;
			}
			
			else{
				uscita.ora=dato.ora;
				uscita.min=dato.min;
				strcpy(uscita.code , dato.code);
			}
		}
	}
	if(count==1){
		fclose(fp);
		ore = uscita.ora - entrata.ora;
		tot= (ore * 60) +(uscita.min - entrata.min);
		printf("il dipendente %s ha lavorato %d minuti.", argv[2], tot);
	}
	else{printf("dipendente non trovaro.");}

	
}
